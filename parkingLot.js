class ParkingLot{
    constructor(number, isAvailable){
        this.number = number;
        this.isAvailable = isAvailable;
    }
}

let parkingLots = [];
let amountOfParkingLot = 5;

function CreateParkingLots(){
    pushSlotsInArray(amountOfParkingLot);
    console.log("Created " + amountOfParkingLot + " parking lots");
}

function pushSlotsInArray(amountOfParkingLot){
    for(let slotCounter = 1; slotCounter <= amountOfParkingLot; slotCounter++){
        const parkingLot = new ParkingLot(slotCounter, false);  
        parkingLots.push(parkingLot);
    }
}

CreateParkingLots();

//For testing only
parkingLots[1].isAvailable = true;

parkingLots.forEach((slot) => {
    if(slot.isAvailable){
        console.log("Slot number "+ slot.number + " is available");
    }
    else{
        console.log("Slot number "+ slot.number + " is not available");
    }
});